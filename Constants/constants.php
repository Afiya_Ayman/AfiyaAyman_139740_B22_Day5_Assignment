<?php

echo "<b>Example 1:</b><br><br>";
define("G","Universal Gravitational Constant = 6.673x10^-11 NM^2/kg2");
echo G;

echo "<hr>";

echo "<b>Example 2:</b><br><br>";

define("PI","3.1416");
echo "Value of PI is ".PI;
