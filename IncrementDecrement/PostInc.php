<?php

echo "<b>Example :</b><br><br>";
$a = 10;
$b = 2.53;

echo "A = ".$a." & B = ".$b."<br>";

echo "The output of a++ is ".$a++;      //10
echo "<br>";
echo "After post increment Value of a = ".$a."<br>"; //11
echo "The output of b++ is ".$b++;      //2.53
echo "<br>";
echo "After post increment Value of b = ".$b; //3.53
