<?php

echo "<b>Example 1:</b><br>";
$a = 10;
$b = 10.00;

var_dump($a,$b);

echo "<br>";

if($a!==$b){
    echo "A & B are not identical";
}
else
{
    echo "A & B are identical";
}

echo "<hr>";

echo "<b>Example 2:</b><br>";
$a = "Ayman";
$b = "Ayman";

var_dump($a,$b);

echo "<br>";

if($a!==$b){
    echo "A & B are not identical";
}
else {
    echo "A & B are identical";
}