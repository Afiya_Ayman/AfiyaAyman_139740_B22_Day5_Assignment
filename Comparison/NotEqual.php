<?php

echo "<b>Example 1:</b><br>";
$a = 10;
$b = 5.0;

echo "A = ".$a."    B = ".$b."<br>";

if($a!=$b){
    echo "TRUE! A is not equal to B";
}
else
{
    echo "False! A is equal to B";
}

echo "<hr>";

echo "<b>Example 2:</b><br>";
$a = 5;
$b = 5;

echo "A = ".$a."    B = ".$b."<br>";

if($a!=$b){

    echo "TRUE! A is not equal to B";
}
else
{
    echo "False! A is equal to B";
}