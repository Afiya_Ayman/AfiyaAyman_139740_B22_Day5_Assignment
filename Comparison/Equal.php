<?php

echo "<b>Example 1:</b><br>";
$a = 10;
$b = 5;

echo "A = ".$a."    B = ".$b."<br>";

if($a==$b){
    echo "A=B";
}
else
{
    echo "A is not equal to B";
}

echo "<hr>";

echo "<b>Example 2:</b><br>";
$a = 5;
$b = 5;

echo "A = ".$a."    B = ".$b."<br>";

if($a==$b){
    echo "A=B";
}
else
{
    echo "A is not equal to B";
}