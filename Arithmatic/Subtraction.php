<?php

$a = 15.0236;
$b = -2.563;

echo nl2br("a = ".$a."  & b = ".$b."\n\n\n");

$c = $a - $b;

echo "The subtraction from a to b is <b>". $c."</b>" ;

echo "<hr>";

$d = -15.0236;
$e = -2.563;

echo nl2br("d = ".$d."  & e = ".$e."\n\n\n");

$f = $d - $e;

echo "The subtraction from d to e is <b>". $f."</b>" ;