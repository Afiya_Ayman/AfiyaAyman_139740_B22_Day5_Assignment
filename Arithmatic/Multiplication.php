<?php

$a = 2.5;
$b = -4;

echo nl2br("a = ".$a."  & b = ".$b."\n\n\n");

$c = $a * $b;

echo "The multiplication of a & b is <b>". $c."</b>" ;

echo "<hr>";

$d = -15.0236;
$e = -2.563;

echo nl2br("d = ".$d."  & e = ".$e."\n\n\n");

$f = $d * $e;

echo "The multiplication of d & e is <b>". $f."</b>" ;

echo "<hr>";
echo "<hr>";
echo "<hr>";

echo "A Program to find out the area of a circle <br>";

$radius = 12;
define("PI",3.1416);

echo "The radius of the circle is ".$radius." <br>";

$area = $radius * PI;

echo "The area of the circle is ".$area;
