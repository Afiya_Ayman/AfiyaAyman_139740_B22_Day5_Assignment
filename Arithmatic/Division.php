<?php

$a = 25;
$b = 1.234;

echo "A contains ".$a." & B contains ".$b."<br>";

$c = $a/$b;

echo "A divided by B (A/B) = ".$c."<hr>";

$a = 0;
$b = 1;

echo "A contains ".$a." & B contains ".$b."<br>";

$c = $a/$b;

echo "A divided by B (A/B) = ".$c."<hr>";

$a = 1;
$b = 0;

echo "A contains ".$a." & B contains ".$b."<br>";

$c = $a/$b;

echo "A divided by B (A/B) = ".$c."<hr>"; //will produce error "Division by zero"
