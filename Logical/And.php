<?php

$a = true;
$b = false;

var_dump($a,$b);
echo "<br>";

if ($a and $b)
{
    echo "Both are true";
}
else
{
    echo "Both of them aren't true";
}

echo "<hr>";

$a = true;
$b = true;
var_dump($a,$b);
echo "<br>";

if ($a and $b)
{
    echo "Both are true";
}
else
{
    echo "Both of them aren't true";
}