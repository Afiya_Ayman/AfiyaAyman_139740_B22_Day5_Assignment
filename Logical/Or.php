<?php

$a = true;
$b = false;

var_dump($a,$b);

if ($a or $b)
{
    echo "<br>Either one of them or Both are true";
}
else
{
    echo "<br>Both of them are false";
}

echo "<hr>";
$a = false;
$b = false;

var_dump($a,$b);

if ($a or $b)
{
    echo "<br>Either one of them or Both are true";
}
else
{
    echo "<br>Both of them are false";
}