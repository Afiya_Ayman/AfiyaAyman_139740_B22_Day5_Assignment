<?php

$a = true;
$b = false;

var_dump($a,$b);
echo "<br>";

if ($a || $b)
{
    echo "<br>Either one of them or Both are true";
}
else {
    echo "<br>Both of them are false";
}

echo "<hr>";

$c = true;
$d = true;
var_dump($c,$d);
echo "<br>";

if ($c || $d)
{
    echo "<br>Either one of them or Both are true";
}
else {
    echo "<br>Both of them are false";
}
