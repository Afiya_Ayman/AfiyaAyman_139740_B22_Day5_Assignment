<?php

$a = true;
$b = false;

var_dump($a,$b);

if ($a xor $b)
{
    echo "<br>Output of xor is true cause Only one of them is true";
}
else
{
    echo "<br>Output of xor is false cause Both of them are false or true";
}


echo "<hr>";
$a = true;
$b = true;

var_dump($a,$b);

if ($a xor $b)
{
    echo "<br>Output of xor is true cause Only one of them is true";
}
else
{
    echo "<br>Output of xor is false cause Both of them are false or true";
}