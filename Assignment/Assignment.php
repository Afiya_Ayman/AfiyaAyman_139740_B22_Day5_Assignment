<?php

$a = 10;

echo "<b> Example 1: </b><br>";

echo "a = ".$a."<br>";

$b = ($a = 3)+9;

echo "b = (a = 3)+9 = ".$b."<br>";
echo "a = ".$a."<br>";

echo "<hr>";
echo "<hr>";

echo "<b> Example 2: </b><br>";

$a = 25.36;

echo "a = ".$a."<br>";

$b = ($a = 25)+9.025;

echo "b = (a = 25)+9.025 = ".$b."<br>";
echo "a = ".$a."<br>";