<?php

$a = 15.0236;
$b = -2.563;

echo nl2br("a = ".$a."  & b = ".$b."\n\n\n");   //nl2br — Inserts HTML line breaks before all newlines in a string

$c = $a - $b;

echo "The subtraction from a to b is <b>". $c."</b>" ;
echo "<br>";

echo nl2br("Welcome\r\nThis is my HTML document", false);

echo "<br>";
echo nl2br("foo isn't\n bar");

echo "<br>";
echo nl2br("aym\n good\n great");