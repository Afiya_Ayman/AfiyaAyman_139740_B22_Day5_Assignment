<?php

function square($i)
{
    return $i*$i;
}


$b = $a = 7;

echo "A = ".$a." & B = ".$b."<br>";
$c = $a++;                              //7
echo "C = ".$c."<br>";                  //7
echo "A = ".$a."<br>";                  //8

echo "E = D = ++B = ".$e = $d = ++$b;   //8

echo "<br>D = ".$d." E = ".$e;           //8

echo "<hr>";
echo $f = square($d++);                 //64

echo "<hr>";

echo square($d);                        //81
echo "<hr>";
echo "F = ".$f."<br>";                  //64
echo "<hr>";
echo $g = square(++$e);                 //81
echo "<hr>";
echo $h = $g += 10;                     //91

echo "<hr>";
echo ++$h+$h++;                         //